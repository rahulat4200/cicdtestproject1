from flask import Blueprint, request, render_template, redirect, url_for
from .models import User
from . import db

main = Blueprint("main", __name__)


@main.route("/")
def home():
    return "Welcome to the User Management App!"


@main.route("/register", methods=["GET", "POST"])
def register():
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        user = User(db.db)
        user.create_user(username, password)
        return redirect(url_for("main.login"))
    return render_template("register.html")


@main.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        username = request.form["username"]
        user = User(db.db)
        if user.find_user(username):
            return "Login Successful!"
        else:
            return "User Not Found!"
    return render_template("login.html")
