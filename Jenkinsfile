pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                git credentialsId: 'gitlab-credentials', url: 'https://gitlab.com/rahulat4200/pythontestapp.git'
            }
        }
        stage('Prepare Environment') {
            steps {
                script {
                    sh label: '', script: 'bash -c "python3 -m venv /var/jenkins_home/venv"'
                }
            }
        }
        stage('Activate Environment and Install Dependencies') {
            steps {
                script {
                    sh label: '', script: 'bash -c "source /var/jenkins_home/venv/bin/activate && pip install -r requirements.txt"'
                }
            }
        }
        stage('Check Python and Pip') {
            steps {
                script {
                    sh 'python3 --version'
                    sh 'pip --version'
                }
            }
        }
        stage('Run Tests') {
            steps {
                sh 'bash -c "source /var/jenkins_home/venv/bin/activate && PYTHONPATH=\$(pwd) pytest"'
            }
        }
        stage('Build Docker Image') {
            steps {
                sh 'docker build -t user-management-app .'
            }
        }
        stage('Upload to Nexus') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'nexus-credentials', usernameVariable: 'NEXUS_USERNAME', passwordVariable: 'NEXUS_PASSWORD')]) {
                    sh 'bash -c "source /var/jenkins_home/venv/bin/activate && twine upload --repository-url http://194.195.115.95:8081/repository/python-hosted/ -u $NEXUS_USERNAME -p $NEXUS_PASSWORD dist/*"'
                }
            }
        }
    }
    post {
        always {
            sh 'echo "This will always run"'
        }
        success {
            sh 'echo "This will run only if successful"'
        }
        failure {
            sh 'echo "This will run only if failed"'
            mail to: 'rahul@rstforum.net',
                 subject: "Failed Pipeline: ${currentBuild.fullDisplayName}",
                 body: "Something is wrong with ${env.BUILD_URL}"
        }
    }
}
